<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\Cost;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class ApiController extends Controller
{
    /**
     * @Route("/costs", methods={"GET"})
     */
    public function tasksAction(Request $request)
    {
        $repository = $this->getDoctrine()
            ->getRepository(Cost::class);

        $dateStart = new\DateTime($request->query->get('dateStart'));
        $dateEnd = new\DateTime($request->query->get('dateEnd'));

        $qb = $repository->createQueryBuilder("e");

        if($request->query->get('dateEnd')) {
            $qb
                ->andWhere('e.date BETWEEN :from AND :to')
                ->setParameter('from', $dateStart )
                ->setParameter('to', $dateEnd);
        } else {
            $qb
                ->andWhere('e.date = :date')
                ->setParameter('date', $dateStart );
        }

        if ($request->query->get('category')) {

            $qb
                ->andWhere('e.category = :category')
                ->setParameter('category', $request->query->get('category') );
        }

        if ($request->query->has('is_cost')) {
            $qb
                ->andWhere('e.is_cost = :is_cost')
                ->setParameter('is_cost', $request->query->get('is_cost') );
        }

        $qb->orderBy('e.date', 'ASC');

        $result = $qb->getQuery()->getResult();

        return $this->json($result);
    }

    /**
     * @Route("/category", methods={"GET"})
     */
    public function categoryAction(Request $request)
    {
        $repository = $this->getDoctrine()
            ->getRepository(Category::class);

        $tasks = $repository->findAll();

        return $this->json($tasks);
    }

    /**
     * @Route("/costs/", methods={"POST"})
     */
    public function costsCreateAction(Request $request)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $task = new Cost();

        $task->setDescription($request->request->get('description'));
        $task->setDate(new \DateTime($request->request->get('date')));
        $task->setPrice($request->request->get('price'));
        $task->setIsCost($request->request->get('is_cost'));

        $category = $this->getDoctrine()
            ->getRepository(Category::class)->find($request->request->get('category'));
        $task->setCategory($category);

        $entityManager->persist($task);
        $entityManager->flush();

        return $this->json($task);
    }

    /**
     * @Route("/statistic", methods={"GET"})
     */
    public function statisticAction(Request $request)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $income =  $entityManager
                ->getRepository(Cost::class)
                ->findby( [ 'is_cost' => 0 ]);


        $test = null;
        foreach ($income as $item) {
            $month = ($item->getDate())->format('n');
            if (isset($test[$month])) {
                $test[$month] += (int)$item->getPrice();
            } else {
                $test[$month] = (int)$item->getPrice();
            }

        }

        return $this->json($test);
    }


}
