import { Line } from 'vue-chartjs'

export default {
  extends: Line,
  data: () => ({
    chartdata: {
      labels: [],
      datasets: [
        {
          label: null,
          backgroundColor: 'rgba(11, 156, 49, 0.2)',
          borderColor: 'rgba(11, 156, 49, 1)',
          data: [],
          lineTension: 0,
          fill: false
        }
      ]
    },
    options: {
      maintainAspectRatio: false,
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      },
      legend: {
        display: true,
        position: 'right'
      }
    },
    summ: 0
  }),

  async mounted () {
    const data = await this.$store.dispatch('fetchStatistic')

    for (let i = 0; i < 12; i++) {
      this.chartdata.labels.push(new Date(2010, i).toLocaleString('ru', {month: 'long'}))
      this.chartdata.datasets[0].data.push(null)
    }

    Object.keys(data).forEach((item) => {
      this.chartdata.datasets[0].data.splice((item - 1), 0, data[+item])
      this.summ += data[+item]
    })
    this.chartdata.datasets[0].label = `Доход ${(this.summ).toLocaleString('ru')}`

    this.renderChart(this.chartdata, this.options)
  }
}
