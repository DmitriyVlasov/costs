import Vue from 'vue'
import Vuex from 'vuex'
import Notifications from 'vue-notification'
import costs from './modules/costs'
import category from './modules/category'
import auth from './modules/auth'
import statistic from './modules/statistic'

Vue.use(Vuex)
Vue.use(Notifications)

const store = new Vuex.Store({
  modules: {
    costs,
    category,
    auth,
    statistic
  }
})

export default store
