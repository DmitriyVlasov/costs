import Vue from 'vue'
import * as api from '../../api/costs'
import Notifications from 'vue-notification'

Vue.use(Notifications)

const costs = {
  state: {
    tasks: {
      payload: [],
      fetching: false,
      error: false
    },
    type: true,
    category: [],
    selectCategory: null,
    dateStart: new Date(),
    dateEnd: null,
    is_cost: null
  },
  actions: {
    fetchTasks ({ state, commit }) {
      commit('setTasksFetching', true)
      commit('setTasksError', false)
      const params = {
        dateStart: state.dateStart.toLocaleDateString(),
        dateEnd: state.dateEnd && state.dateEnd.toLocaleDateString(),
        category: state.selectCategory,
        is_cost: state.is_cost
      }
      api
        .query(params)
        .then(response => {
          const tasks = response.data
          commit('setTasksFetching', false)
          commit('setTasks', tasks)
        })
        .catch(() => {
          commit('setTasksFetching', false)
          commit('setTasksError', true)
          Vue.notify({
            title: 'Ошибка',
            text: 'Произошла ошибка загруки',
            type: 'error'
          })
        })
    },
    addCost ({ state, dispatch }, payload) {
      const params = { ...payload.params, is_cost: +state.type }
      api
        .create(params)
        .then(() => {
          dispatch('fetchTasks')
          payload.modal.hide()
          Vue.notify({
            title: 'Новая запись добавлена',
            type: 'success'
          })
        })
        .catch(() => {
          Vue.notify({
            title: 'Ошибка',
            text: 'Произошла ошибка загруки',
            type: 'error'
          })
        })
    }
  },
  mutations: {
    setTasks (state, tasks) {
      state.tasks.payload = tasks.map(item => {
        item.checked = false
        return item
      })
    },
    setTasksFetching (state, status) {
      state.tasks.fetching = status
    },
    setTasksError (state, status) {
      state.tasks.error = status
    },
    setTypeModal (state, type) {
      state.type = type
    },
    setDateStart (state, dateStart) {
      state.dateStart = dateStart
    },
    setDateEnd (state, dateEnd) {
      state.dateEnd = dateEnd
    },
    setSelectCategory (state, selectCategory) {
      state.selectCategory = selectCategory
    },
    setIsCost (state, isCost) {
      state.is_cost = isCost
    }
  }
}

export default costs
