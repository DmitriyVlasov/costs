import * as api from '../../api/statistic'

const statistic = {
  actions: {
    async fetchStatistic ({commit}) {
      const { data } = await api.query()
      return data
    }
  }
}

export default statistic
