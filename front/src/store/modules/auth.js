import Vue from 'vue'
import * as api from '../../api/auth'
import router from '../../router'
import Notifications from 'vue-notification'

Vue.use(Notifications)

const costs = {

  state: {
    username: ''
  },

  actions: {
    loginCheck ({ commit }, data) {
      api
        .login(data)
        .then(response => {
          localStorage.setItem('token', response.data.token)
          commit('setUser', data.username)
          router.push({name: 'costs'})
        })
        .catch(() => {
          Vue.notify({
            title: 'Ошибка',
            text: 'Произошла ошибка загруки',
            type: 'error'
          })
        })
    }
  },

  mutations: {
    setUser (state, username) {
      state.username = username
    }
  }
}

export default costs
