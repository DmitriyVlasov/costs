import Vue from 'vue'
import * as api from '../../api/category'
import Notifications from 'vue-notification'

Vue.use(Notifications)

const costs = {
  state: {
    categories: []
  },
  actions: {
    fetchCategory ({commit}) {
      api
        .query()
        .then(response => {
          const payload = response.data
          commit('setAll', payload)
        })
        .catch(() => {
          Vue.notify({
            title: 'Ошибка',
            text: 'Произошла ошибка загруки',
            type: 'error'
          })
        })
    }
    // addCost ({state, dispatch}, payload) {
    //   const params = {...payload.params, is_cost: +state.type}
    //   api
    //     .create(params)
    //     .then(() => {
    //       dispatch('fetchTasks')
    //       payload.modal.hide()
    //       Vue.notify({
    //         title: 'Новая запись добавлена',
    //         type: 'success'
    //       })
    //     })
    //     .catch(() => {
    //       Vue.notify({
    //         title: 'Ошибка',
    //         text: 'Произошла ошибка загруки',
    //         type: 'error'
    //       })
    //     })
    // }
  },
  mutations: {
    setAll (state, payload) {
      state.categories = payload.map(item => {
        return { ...item, value: item.id, text: item.name }
      })
    }
  }
}

export default costs
