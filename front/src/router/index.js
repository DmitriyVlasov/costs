import Vue from 'vue'
import Router from 'vue-router'
import Content from '@/views/index'
import Auth from '@/components/Auth/Auth'
import Statistic from '@/views/Statistic'
import Costs from '@/views/Costs'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/lk/costs'
    },
    {
      path: '*',
      redirect: '/lk/costs'
    },
    {
      path: '/auth',
      name: 'auth',
      component: Auth
    },

    {
      path: '/lk',
      name: 'lk',
      component: Content,
      children: [
        {
          path: 'costs',
          name: 'costs',
          component: Costs
        },

        {
          path: 'statistic',
          name: 'statistic',
          component: Statistic
        }
      ]
    }
  ]
})
