import * as axios from 'axios'

import router from '../router/'

export const instance = axios.create({
  baseURL: 'http://localhost:8000/api'
})

instance.interceptors.request.use(
  config => {
    if (localStorage.getItem('token')) {
      config.headers.authorization = 'Bearer ' + localStorage.getItem('token')
    }
    return config
  },
  error => Promise.reject(error)
)

instance.interceptors.response.use(
  response => response,
  (error) => {
    const { response } = error

    if (response.status === 401) {
      router.push({ name: 'auth' })
    }

    if (response.status === 403) {
      router.push({ name: 'auth' })
    }

    return Promise.reject(error)
  }
)
