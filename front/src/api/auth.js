
import * as axios from 'axios'

export const instance = axios.create({
  baseURL: 'http://localhost:8000/'
})

export const login = params => instance.post('/login_check', params, {
  headers: {
    'content-type': 'application/json'
  }
})
