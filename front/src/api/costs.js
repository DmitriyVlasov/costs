import { instance } from './api'
import qs from 'qs'

export const query = params => instance.get('/costs', { params })

export const create = data => instance.post('/costs/', qs.stringify(data))

export const deleteTask = id => instance.delete(`/task/${id}/`)

export const changeTime = data => instance.post(`/tasks/time/`, qs.stringify(data))
